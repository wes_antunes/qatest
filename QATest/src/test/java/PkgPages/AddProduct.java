package PkgPages;

import org.openqa.selenium.*;

public class AddProduct {
    WebDriver driver;

    By menu = By.className("topnav-cookware");
    By banner = By.className("stickyOverlayMinimizeButton");
    By submenu = By.linkText("Cookware Sets");
    By product = By.xpath("//*[@id=\"subCatListContainer\"]/ul/li[4]/div[1]/div/a/span[2]/img");
    By addCart = By.xpath("//button[@id='primaryGroup_addToCart_0']");
    By checkoutButton = By.id("anchor-btn-checkout");

    public AddProduct(WebDriver driver) {this.driver = driver;}

    public void clickMenu() {driver.findElement(menu).click();}

    public void clickSubMenu() {driver.findElement(submenu).click();}

    public void clickProduct() {driver.findElement(product).click();}

    public void clickCart() {driver.findElement(addCart).click(); }

    public void clickCheckout() {driver.findElement(checkoutButton).click();}

    public void clickBanner(){driver.findElement(banner).click();}

}
