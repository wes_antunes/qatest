package PkgPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class FindProduct {

    WebDriver driver;

    By srcProduct = By.xpath("//*[@id=\"search-field\"]");
    By fechaBanner = By.xpath("/html[1]/body[1]/div[8]/div[1]/a[1]");
    By clickLook = By.xpath("//*[@id=\"subCatListContainer\"]/ul/li[1]/a[1]/span");

    public FindProduct(WebDriver driver) {this.driver = driver;}

    public void strPesquisa(String produto) {driver.findElement(srcProduct).sendKeys(produto);}

    public void clickFecha() {driver.findElement(fechaBanner).click();}

    public void clickEnter() {driver.findElement(By.id("Value")).sendKeys(Keys.ENTER);}

    public void clickQKLook() {driver.findElement(clickLook).click();}

}



