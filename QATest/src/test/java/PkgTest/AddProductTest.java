package PkgTest;

import static org.junit.Assert.*;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import PkgPages.AddProduct;
import java.util.concurrent.TimeUnit;


public class AddProductTest {
    @Test
    public void testCase() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.get("https://www.williams-sonoma.com/");
        AddProduct objProduct = new AddProduct(driver);
        objProduct.clickBanner();
        objProduct.clickMenu();
        objProduct.clickSubMenu();
        objProduct.clickProduct();
        objProduct.clickCart();
        WebElement addCart =  driver.findElement(By.xpath("//*[@id=\"primaryGroup_addToCart_0\"]"));
        String textoCart = addCart.getText();
        assertEquals ("Não há o botão", textoCart,"ADD TO CART");
        objProduct.clickCheckout();
        WebElement btnCheckout =  driver.findElement(By.xpath("//*[@id=\"anchor-btn-checkout\"]"));
        String textoCkt = btnCheckout.getText();
        assertEquals ("Não há o botão", textoCkt,"CHECKOUT");
        WebElement txtProduct =  driver.findElement(By.xpath("//*[@id=\"title\"]"));
        String textoProdct = txtProduct.getText();
        assertEquals ("Não é o mesmo produto", textoProdct,"Calphalon Premier Space-Saving Hard-Anodized Nonstick 10-Piece Cookware Set");
        driver.quit();

    }

}
