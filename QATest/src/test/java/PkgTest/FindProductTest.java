package PkgTest;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import PkgPages.FindProduct;

public class FindProductTest{
    @Test
    public void testCase(){

        System.setProperty("webdriver.gecko.driver", "c:\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.williams-sonoma.com/");
        FindProduct objFindProduct = new FindProduct(driver);
        objFindProduct.clickFecha();
        objFindProduct.strPesquisa("fry pan");
        objFindProduct.clickEnter();
        if (driver.getPageSource().contains("QUICKLOOK")) {
            System.out.println("Contém o botão QUICKLOOK");
        } else {
            System.out.println("Não contém o botão QUICKLOOK");
        }
        objFindProduct.clickQKLook();
        if (driver.getPageSource().contains("Sugg. Price $1,329")){
            System.out.println("Contém o mesmo preço");
        } else {
            System.out.println("Não contém o mesmo preço");
        }
        if (driver.getPageSource().contains("All-Clad d5 Stainless-Steel 10-Piece Cookware Set")){
            System.out.println("É o mesmo produto");
        } else {
            System.out.println("Não é o mesmo produto");
        }
        driver.quit();

    }



}

